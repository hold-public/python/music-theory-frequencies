def transpose_down(freq):
    freq_transposed = freq
    while freq_transposed > 2.0:
        freq_transposed /= 2.0
    return freq_transposed

def calc_freqs_rational():
    freq = ((2 ** (1 / 12)) ** 5) # F
    freqs_rational = [freq]
    for _ in range(6):
        freq *= (3 / 2)
        freqs_rational.append(transpose_down(freq))
    freqs_rational.append(2.0)
    freqs_rational.sort()
    return freqs_rational

def calc_freqs_equal_temp():
    steps = ['F', 'F', 'H', 'F', 'F','F', 'H'] # Major scale
    freq = 1.0
    freqs_equal_temp = [freq]
    for step in steps:
        if step == 'F':
            steps = 2
        else:
            steps = 1
        freq *= ((2 ** (1 / 12)) ** steps)
        freqs_equal_temp.append(freq)
    return freqs_equal_temp

freqs_rational = calc_freqs_rational()
freqs_equal_temp = calc_freqs_equal_temp()

for freq_rational, freq_equal_temp in zip(freqs_rational, freqs_equal_temp):
    print(f'{freq_rational:.3f} (rational) vs {freq_equal_temp:.3f} (equal-tempered)')
